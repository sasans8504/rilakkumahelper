import punch.LoginWindow;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {
//        try {
//            new PunchCard().login();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        String date = "2020/01/15";
//        String time = "09:18/其他-八樓大門";
//        time = time.split("/")[0];
//        String calendar = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
//        System.out.println("calendar = " + calendar);
//        if(date.equalsIgnoreCase(calendar)){
//            System.out.println("date is right");
//            int clock = Integer.parseInt(time.split(":")[0]);
//            System.out.println("punch in time = " + clock);
//            clock += 9;
//            time = clock + time.split(":")[1];
//            System.out.println("punch out time = " + time);
//        }else{
//            System.out.println("can't get punch in today, check if something wrong?");
//            System.out.println("calendar = " + calendar);
//            System.out.println("date = " + date);
//        }
        JFrame frame = new JFrame();
        frame.setSize(280, 170);
        frame.setResizable(false);
        LoginWindow.INSTANCE.init();
        JButton button = new JButton("test");
        button.addActionListener(e -> LoginWindow.INSTANCE.frame.setVisible(true));
        frame.add(button);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}