package bin;

import utils.Vector2;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.Timer;

import static utils.Properties.*;

public class RilakkumaAI extends TimerTask {
    RilakkumaCore core;
    public int delay = 0;
    public int period = 500;
    public JButton rilakkuma;
    public Vector2 position;
    Map<String, Icon> gifs = new HashMap<>();

    enum Direction {LEFT, RIGHT}

    Direction direction = Direction.LEFT;

    public RilakkumaAI(RilakkumaCore core) {
        this.core = core;
        rilakkuma = new JButton();
        loadImage();
        //button background transparent
        rilakkuma.setOpaque(false);
        rilakkuma.setContentAreaFilled(false);
        rilakkuma.setBorderPainted(false);
        rilakkuma.setFocusPainted(false);
        rilakkuma.setBackground(new Color(0, 0, 0, 0));

        position = new Vector2();
    }

    public void start() {
        Timer timer = new Timer();
        timer.schedule(this, delay, period);
    }

    @Override
    public void run() {
        if(core.isReload()){
            core.toLastStaticStatus();
            loadImage();
        }
        if (core.getResetPosition()) {
            do {
                rilakkuma.setLocation(0, 0);
            } while (rilakkuma.getLocation().x != 0 && rilakkuma.getLocation().y != 0);
            core.doneResetPosition();
        }
        if (!core.isDragging()) {
            if (core.isMoving()) {
                if (direction == Direction.LEFT) {
                    rilakkuma.setLocation(rilakkuma.getLocation().x - 2, rilakkuma.getLocation().y);
                } else if (direction == Direction.RIGHT) {
                    rilakkuma.setLocation(rilakkuma.getLocation().x + 2, rilakkuma.getLocation().y);
                }

                if (rilakkuma.getX() < 0) {
                    direction = Direction.RIGHT;
                    core.move();
                } else if (rilakkuma.getX() > core.width - 400) {
                    direction = Direction.LEFT;
                    core.move();
                }
            }

            if (core.isChange()) {
                if (core.isGifStatusMove()) {
                    if (direction.equals(Direction.LEFT)) {
                        if(core.isSpecial())
                            rilakkuma.setIcon(gifs.get(IMAGE_SPECIAL_LEFT));
                        else
                            rilakkuma.setIcon(gifs.get(IMAGE_LEFT));
                    }else {
                        if(core.isSpecial())
                            rilakkuma.setIcon(gifs.get(IMAGE_SPECIAL_RIGHT));
                        else
                            rilakkuma.setIcon(gifs.get(IMAGE_RIGHT));
                    }
                } else if (core.isGifStatusSit()) {
                    if (direction.equals(Direction.LEFT))
                        rilakkuma.setIcon(gifs.get(IMAGE_SIT));
                    else
                        rilakkuma.setIcon(gifs.get(IMAGE_SIT_RIGHT));
                } else if (core.isGifStatusNetworking()) {
                    rilakkuma.setIcon(gifs.get(IMAGE_NETWORKING));
                } else if (core.isGifStatusSuccess()) {
                    rilakkuma.setIcon(gifs.get(IMAGE_SUCCESS));
                } else if (core.isGifStatusFailed()) {
                    rilakkuma.setIcon(gifs.get(IMAGE_FAILED));
                }
                core.gifChanged();
            }
            position.setPosition(rilakkuma.getLocation());
        }
    }
    
    private void loadImage(){
        for (String key :
                IMAGE_RESOURCES) {
            if (key.equals(IMAGE_ICON) || key.equals(IMAGE_COUNTDOWN)) {
                continue;
            }else if(key.equals(IMAGE_SPECIAL)){
                if (theme.equals(IMAGE_CHAIRO)) {
                    gifs.put(IMAGE_SPECIAL_LEFT, new ImageIcon(resources.get(IMAGE_SPECIAL_LEFT)));
                    gifs.put(IMAGE_SPECIAL_RIGHT, new ImageIcon(resources.get(IMAGE_SPECIAL_RIGHT)));
                    continue;
                }
            }
            gifs.put(key, new ImageIcon(resources.get(key)));
        }
        rilakkuma.setIcon(gifs.get(IMAGE_LEFT));
    }
}
