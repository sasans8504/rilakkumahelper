package bin;

import utils.Properties;

import java.awt.*;
import javax.swing.*;

public enum RilakkumaCore {
    INSTANCE;
    public int width = 0;
    public int height = 0;
    public int locationX = 0;

    enum Status {WAIT, MOVE, CHANGE, SIT, NETWORKING, SUCCESS, FAILED, RELOAD}

    private Status status = Status.MOVE;
    private Status lastStaticStatus = Status.MOVE;
    private Status gifStatus = Status.MOVE;
    private boolean isDragging = false;
    private boolean resetPosition = false;
    private boolean special = false;
    JWindow window;
    RilakkumaKeyEvent rilakkumaKeyEvent;
    RilakkumaAI rilakkumaAI;
    RilakkumaTray rilakkumaTray;

    public void run() {
        Properties.INSTANCE.init();
        window = new JWindow();
        window.setLayout(new GridBagLayout());

        //get multi monitor width, only use second screen
        GraphicsDevice[] gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
        for (GraphicsDevice gdt : gd) {
            Rectangle bounds = gdt.getDefaultConfiguration().getBounds();
            System.out.println(bounds.toString());
            width += bounds.width;
            if(bounds.height > height) {
                height = bounds.height;
            }
            if(bounds.x < 0)
                locationX += bounds.x;
        }

        window.setSize(width, height);
        window.setLocationRelativeTo(null);
        rilakkumaAI = new RilakkumaAI(this);
        rilakkumaKeyEvent = new RilakkumaKeyEvent(rilakkumaAI.rilakkuma);
        rilakkumaAI.start();
        window.add(rilakkumaAI.rilakkuma);

        window.setBackground(new Color(0, 0, 0, 0));
        window.setLocation(locationX, 0);
        window.setAlwaysOnTop(true);
        rilakkumaTray = new RilakkumaTray();
        window.setVisible(true);
    }

    public void dispose() {
        rilakkumaTray.trayRemove();
        System.exit(0);
    }

    public void traySwitch(int mode) {
        rilakkumaTray.switchMode(mode);
    }

    public void sit() {
        status = Status.CHANGE;
        gifStatus = Status.SIT;
        lastStaticStatus = Status.SIT;
    }

    public void move() {
        status = Status.CHANGE;
        gifStatus = Status.MOVE;
        lastStaticStatus = Status.MOVE;
    }

    public void gifChanged() {
        status = gifStatus;
    }

    public void networking() {
        status = Status.CHANGE;
        gifStatus = Status.NETWORKING;
    }

    public void success() {
        status = Status.CHANGE;
        gifStatus = Status.SUCCESS;
    }

    public void failed() {
        status = Status.CHANGE;
        gifStatus = Status.FAILED;
    }

    public void special(){
        special = !special;
        move();
    }

    public void standby() {
        status = Status.WAIT;
    }

    public void reload() {
        status = Status.RELOAD;
        lastStaticStatus = Status.MOVE;
    }

    public boolean isGifStatusMove() {
        return gifStatus == Status.MOVE;
    }

    public boolean isGifStatusSit() {
        return gifStatus == Status.SIT;
    }

    public boolean isGifStatusNetworking() {
        return gifStatus == Status.NETWORKING;
    }

    public boolean isGifStatusSuccess() {
        return gifStatus == Status.SUCCESS;
    }

    public boolean isGifStatusFailed() {
        return gifStatus == Status.FAILED;
    }

    public boolean isDragging() {
        return isDragging;
    }

    public void setDragging(boolean dragging) {
        isDragging = dragging;
    }

    public boolean isSit() {
        return status == Status.SIT;
    }

    public boolean isMoving() {
        return status == Status.MOVE;
    }

    public boolean isChange() {
        return status == Status.CHANGE;
    }

    public boolean isNetworking() {
        return status == Status.NETWORKING;
    }

    public boolean isSuccess() {
        return status == Status.SUCCESS;
    }

    public boolean isFailed() {
        return status == Status.FAILED;
    }

    public boolean isReload(){
        return status == Status.RELOAD;
    }

    public boolean isSpecial(){ return special; }

    public void toLastStaticStatus() {
        if (lastStaticStatus == Status.MOVE) {
            move();
        } else {
            sit();
        }
    }

    public void reloadImageAndSize(String theme, String size){
        Properties.loadResources(theme, size);
        reload();
    }

    public void reloadImage(String theme) {
        Properties.loadResources(theme);
        reload();
    }

    public void doResetPosition(){
        resetPosition = true;
        window.setLocation(locationX, 0);
    }

    public boolean getResetPosition(){
        return resetPosition;
    }

    public void doneResetPosition(){
        resetPosition = false;
    }
}