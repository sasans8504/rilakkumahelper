package bin;

import javax.swing.*;

public class RilakkumaHelper {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(RilakkumaCore.INSTANCE::run);
    }
}
