package bin;

import utils.CelebrateEvent;
import utils.Properties;
import utils.RDialog;

import java.awt.*;
import java.awt.event.*;

public class RilakkumaKeyEvent extends Component implements MouseListener, MouseMotionListener, KeyListener {
    RilakkumaCore core = RilakkumaCore.INSTANCE;
    Component button;
    MouseEvent pressed;

    public RilakkumaKeyEvent(Component button) {
        this.button = button;
        button.addMouseListener(this);
        button.addMouseMotionListener(this);
        button.addKeyListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() != MouseEvent.BUTTON2) {
            if (e.getButton() == MouseEvent.BUTTON3) {
                if (Properties.isEvent131()) {
                    if(!core.isSpecial())
                        RDialog.showMessageDialog(button, "最愛阿果果 一周年快樂!!");
                }
                core.special();
            } else {
                if (core.isMoving()) {
                    core.sit();
                    core.traySwitch(RilakkumaTray.ACTION_SIT);
                } else if (core.isSit()) {
                    core.move();
                    core.traySwitch(RilakkumaTray.ACTION_MOVE);
                }
            }
        } else {
            core.dispose();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        pressed = e;
        core.setDragging(true);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        core.setDragging(false);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int x = button.getX() - pressed.getX() + e.getX();
        int y = button.getY() - pressed.getY() + e.getY();
        button.setLocation(x, y);
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
