package bin;

import net.sf.json.JSONObject;
import punch.LoginWindow;
import punch.PunchCard;
import punch.ScheduleCheck;
import search.SearchComic;
import utils.Properties;
import utils.RClock;
import utils.RDialog;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static utils.Properties.*;

public class RilakkumaTray {
    RilakkumaCore core = RilakkumaCore.INSTANCE;
    SystemTray systemTray;
    TrayIcon trayIcon;
    CheckboxMenuItem[] actionMode = new CheckboxMenuItem[2];
    CheckboxMenuItem[] theme = new CheckboxMenuItem[2];
    private static Map<String, MenuItem> lockMap = new HashMap<>();
    public static final int ACTION_MOVE = 0;
    public static final int ACTION_SIT = 1;

    public RilakkumaTray() {
        if (SystemTray.isSupported()) {
            systemTray = SystemTray.getSystemTray();
            Image trayIconImage = Toolkit.getDefaultToolkit().getImage(resources.get(IMAGE_ICON));
            PopupMenu popupMenu = new PopupMenu();
            MenuItem findItem = new MenuItem("找回" + Properties.theme);
            findItem.addActionListener(e -> core.doResetPosition());
            MenuItem hideItem = new MenuItem("躲起來");
            hideItem.addActionListener(e -> {
                core.window.setVisible(!core.window.isVisible());
                if (!core.window.isVisible())
                    hideItem.setLabel("跳出來");
                else
                    hideItem.setLabel("躲起來");
            });
            MenuItem exitItem = new MenuItem("結束");
            exitItem.addActionListener(e -> {
                systemTray.remove(trayIcon);
                System.exit(0);
            });

            popupMenu.add(findItem);
            popupMenu.add(hideItem);
            popupMenu.add(clockMenuG("鬧鐘"));
            popupMenu.add(themeMenuG("主題選擇", findItem));
            popupMenu.add(sizeMenuG("大小選擇"));
            popupMenu.add(actionMenuG("模式選擇"));
            popupMenu.add("-");
            popupMenu.add(punchMenuG("下班提醒"));
            popupMenu.add(comicMenuG("漫畫追蹤"));
            popupMenu.add("-");
            popupMenu.add(exitItem);

            trayIcon = new TrayIcon(trayIconImage, Properties.theme, popupMenu);
            trayIcon.setImageAutoSize(true);
            try {
                systemTray.add(trayIcon);
            } catch (AWTException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("系統不支持縮小至系統列");
            System.exit(0);
        }
    }

    public void switchMode(int mode) {
        for (int i = 0; i < actionMode.length; i++) {
            if (i == mode) {
                actionMode[i].setState(true);
                System.out.println(actionMode[i].getLabel() + "selected");
                continue;
            }
            actionMode[i].setState(false);
        }
    }

    public void trayRemove() {
        systemTray.remove(trayIcon);
    }

    public static void lockMenu(String key) {
        lockMap.get(key).setEnabled(false);
    }

    public static void unlockMenu(String key) {
        lockMap.get(key).setEnabled(true);
    }

    private void reloadIcon() {
        trayIcon.setImage(Toolkit.getDefaultToolkit().getImage(resources.get(IMAGE_ICON)));
    }

    private Menu actionMenuG(String label){
        Menu temp = new Menu(label);
        ItemListener actionItemListener = e -> {
            actionMode[ACTION_MOVE].setState(false);
            actionMode[ACTION_SIT].setState(false);
            if (e.getStateChange() == ItemEvent.SELECTED || e.getStateChange() == ItemEvent.DESELECTED) {
                System.out.println(((CheckboxMenuItem) e.getSource()).getLabel() + " selected");
                ((CheckboxMenuItem) e.getSource()).setState(true);
            }

            if (actionMode[ACTION_MOVE].getState()) {
                core.move();
            } else if (actionMode[ACTION_SIT].getState()) {
                core.sit();
            }
        };
        actionMode[ACTION_MOVE] = new CheckboxMenuItem("移動", true);
        actionMode[ACTION_MOVE].addItemListener(actionItemListener);
        actionMode[ACTION_SIT] = new CheckboxMenuItem("坐下", false);
        actionMode[ACTION_SIT].addItemListener(actionItemListener);
        temp.add(actionMode[ACTION_MOVE]);
        temp.add(actionMode[ACTION_SIT]);
        return temp;
    }

    private Menu themeMenuG(String label, MenuItem findItem){
        Menu temp = new Menu(label);
        ItemListener themeItemListener = e -> {
            theme[0].setState(false);
            theme[1].setState(false);
            if (e.getStateChange() == ItemEvent.SELECTED || e.getStateChange() == ItemEvent.DESELECTED) {
                System.out.println(((CheckboxMenuItem) e.getSource()).getLabel() + " selected");
                ((CheckboxMenuItem) e.getSource()).setState(true);
            }

            if (theme[0].getState()) {
                core.reloadImage(IMAGE_CHAIRO);
                writeC(DATA_THEME, IMAGE_CHAIRO);
            } else if (theme[1].getState()) {
                core.reloadImage(IMAGE_CAPOO);
                writeC(DATA_THEME, IMAGE_CAPOO);
            }
            reloadIcon();
            findItem.setLabel("找回"+Properties.theme);
        };
        theme[0] = new CheckboxMenuItem("小熊熊", true);
        theme[0].addItemListener(themeItemListener);
        theme[1] = new CheckboxMenuItem("卡波", false);
        theme[1].addItemListener(themeItemListener);
        temp.add(theme[ACTION_MOVE]);
        temp.add(theme[ACTION_SIT]);
        if (Properties.theme.equals(IMAGE_CAPOO)) {
            theme[0].setState(false);
            theme[1].setState(true);
        }
        return temp;
    }

    private Menu clockMenuG(String label){
        Menu temp = new Menu(label);
        CheckboxMenuItem countdown = new CheckboxMenuItem("倒數計時", false);
        ItemListener clockItemListener = e -> {
            if (e.getStateChange() == ItemEvent.SELECTED || e.getStateChange() == ItemEvent.DESELECTED) {
                System.out.println(((CheckboxMenuItem) e.getSource()).getLabel() + " selected");
                ((CheckboxMenuItem) e.getSource()).setState(true);
            }

            RClock.getInstance();
        };
        countdown.addItemListener(clockItemListener);
        temp.add(countdown);
        return temp;
    }

    private Menu punchMenuG(String label){
        Menu temp = new Menu(label);
        lockMap.put(MENU_PUNCH, temp);
        MenuItem checkPunchOutTimeItem = new MenuItem(getMsgDATE()+"下班時間:"+getMsgOUT());
        MenuItem punchSettingItem = new MenuItem("設定帳號密碼");
        punchSettingItem.addActionListener(e -> LoginWindow.getInstance());
        CheckboxMenuItem autoPunchOutTimeItem = new CheckboxMenuItem("下班提醒");
        autoPunchOutTimeItem.addItemListener(e -> {
            if (isUserInfoEmpty()) {
                int input = RDialog.showConfirmDialog(null, "尚未設定登入帳號密碼，請問要現在設定嗎?", "小咪說", RDialog.OK_CANCEL_OPTION);
                if (input == RDialog.OK_OPTION) LoginWindow.getInstance();
                ((CheckboxMenuItem) e.getSource()).setState(false);
            } else {
                setAutoCheckOutTime(e.getStateChange() == ItemEvent.SELECTED);
                if (isAutoCheckOutTime()) {
                    if (!ScheduleCheck.isRunning()) {
                        ScheduleCheck.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
                        ScheduleCheck.scheduledExecutorService.schedule(new ScheduleCheck(checkPunchOutTimeItem, getMsgDATE()+"下班時間:"+getMsgOUT()), ScheduleCheck.getInitDelay(), TimeUnit.SECONDS);
                    }
                } else {
                    ScheduleCheck.scheduledExecutorService.shutdown();
                }
            }
        });
        if (isAutoCheckOutTime()) {
            autoPunchOutTimeItem.setState(true);
            ScheduleCheck.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            ScheduleCheck.scheduledExecutorService.schedule(new ScheduleCheck(checkPunchOutTimeItem, getMsgDATE()+"下班時間:"+getMsgOUT()), ScheduleCheck.getInitDelay(), TimeUnit.SECONDS);
        }
        MenuItem getPunchOutTimeItem = new MenuItem("強制取得時間");
        getPunchOutTimeItem.addActionListener(e -> {
            ScheduleCheck.scheduledExecutorService.shutdown();
            ScheduleCheck.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            ScheduleCheck.scheduledExecutorService.schedule(new ScheduleCheck(checkPunchOutTimeItem, getMsgDATE()+"下班時間:"+getMsgOUT(), true), ScheduleCheck.getInitDelay(), TimeUnit.SECONDS);
        });
        temp.add(punchSettingItem);
        temp.add("-");
        temp.add(autoPunchOutTimeItem);
        temp.add(getPunchOutTimeItem);
        temp.add(checkPunchOutTimeItem);

        return temp;
    }

    private Menu comicMenuG(String label){
        Menu temp = new Menu(label);
        MenuItem onePunchMan = new MenuItem("一拳超人" + getOnePunchManEpisode());
        lockMap.put(MENU_COMIC_ONEPUNCHMAN, onePunchMan);
        onePunchMan.addActionListener(e -> {
            ExecutorService thread = Executors.newSingleThreadExecutor();
            thread.execute(SearchComic::onePunchMan);
            onePunchMan.setLabel("一拳超人" + getOnePunchManEpisode());
        });
        MenuItem onePiece = new MenuItem("海賊王" + getOnePieceEpisode());
        lockMap.put(MENU_COMIC_ONEPIECE, onePiece);
        onePiece.addActionListener(e -> {
            ExecutorService thread = Executors.newSingleThreadExecutor();
            thread.execute(SearchComic::onePiece);
            onePiece.setLabel("海賊王" + getOnePieceEpisode());
        });

        temp.add(onePunchMan);
        temp.add(onePiece);
        return temp;
    }

    private Menu sizeMenuG(String label){
        Menu temp = new Menu(label);
        CheckboxMenuItem large = new CheckboxMenuItem("大", false);
        CheckboxMenuItem middle = new CheckboxMenuItem("中", false);
        CheckboxMenuItem small = new CheckboxMenuItem("小", false);
        ItemListener actionItemListener = e -> {
            large.setState(false);
            middle.setState(false);
            small.setState(false);
            if (e.getStateChange() == ItemEvent.SELECTED || e.getStateChange() == ItemEvent.DESELECTED) {
                System.out.println(((CheckboxMenuItem) e.getSource()).getLabel() + " selected");
                ((CheckboxMenuItem) e.getSource()).setState(true);
            }

            if (large.getState()) {
                core.reloadImageAndSize(Properties.theme, IMAGE_SIZE_LARGE);
                writeC(DATA_THEME, Properties.theme);
                writeC(DATA_THEME_SIZE, IMAGE_SIZE_LARGE);
            } else if (middle.getState()) {
                core.reloadImageAndSize(Properties.theme, IMAGE_SIZE_MIDDLE);
                writeC(DATA_THEME, Properties.theme);
                writeC(DATA_THEME_SIZE, IMAGE_SIZE_MIDDLE);
            } else if (small.getState()) {
                core.reloadImageAndSize(Properties.theme, IMAGE_SIZE_SMALL);
                writeC(DATA_THEME, Properties.theme);
                writeC(DATA_THEME_SIZE, IMAGE_SIZE_SMALL);
            }
        };
        large.addItemListener(actionItemListener);
        middle.addItemListener(actionItemListener);
        small.addItemListener(actionItemListener);
        temp.add(large);
        temp.add(middle);
        temp.add(small);
        if (themeSize.equals(IMAGE_SIZE_LARGE)) {
            large.setState(true);
        }else if (themeSize.equals(IMAGE_SIZE_MIDDLE)) {
            middle.setState(true);
        }else if(themeSize.equals(IMAGE_SIZE_SMALL)) {
            small.setState(true);
        }
        return temp;
    }
}
