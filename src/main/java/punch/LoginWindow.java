package punch;

import utils.Properties;
import utils.RDialog;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class LoginWindow extends JFrame {
    private static LoginWindow instance;

    public static LoginWindow getInstance() {
        if(instance == null) instance = new LoginWindow();
        else if(!instance.isVisible()) instance.setVisible(true);
        return instance;
    }

    LoginWindow() {
        setSize(280, 240);
        setResizable(false);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        JLabel apolloIcon = new JLabel(new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("apolloxe.png"))));
        apolloIcon.setBounds(10, 10, 230, 60);
        JLabel labelAccount = new JLabel("帳號：");
        labelAccount.setBounds(10, 70, 100, 30);
        JLabel labelAccountHint = new JLabel("只需帳號 會自動填入@gamefortunetech.com");
        labelAccountHint.setBounds(10, 90, 230, 30);
        labelAccountHint.setFont(new Font("Dialog", Font.PLAIN, 10));
        JLabel labelPassword = new JLabel("密碼：");
        labelPassword.setBounds(10, 115, 100, 30);
        JTextField account = new JTextField();
        account.setBounds(50, 70, 200, 30);
        JPasswordField password = new JPasswordField();
        password.setBounds(50, 115, 200, 30);
        password.setEchoChar('*');
        JButton enter = new JButton("確定");
        enter.setBounds(25, 155, 60, 30);
        enter.addActionListener(e -> {
            if(account.getText().contains("@gamefortunetech.com")){
                account.setText(account.getText().split("@")[0]);
                RDialog.showMessageDialog(this, "無需填入後綴，已經自動刪除");
            }
            String status = Properties.saveUserInfo(account.getText()+"@gamefortunetech.com", password.getPassword());
            if(Properties.STATUS_SUCCESS.equals(status)){
                RDialog.showMessageDialog(this, "儲存成功");
                setVisible(false);
            }else{
                RDialog.showMessageDialog(this, "儲存失敗，請波波檢查");
            }
        });
        JButton cancel = new JButton("取消");
        cancel.setBounds(105, 155, 60, 30);
        cancel.addActionListener(e -> {
            account.setText(Properties.getSavedAccount().split("@")[0]);
            password.setText(Properties.getSavedPassword());
            this.dispose();
        });
        account.setText(Properties.getSavedAccount().split("@")[0]);
        password.setText(Properties.getSavedPassword());
        panel.add(apolloIcon);
        panel.add(labelAccount);
        panel.add(labelAccountHint);
        panel.add(account);
        panel.add(labelPassword);
        panel.add(password);
        panel.add(enter);
        panel.add(cancel);
        add(panel);
        setVisible(true);
    }
}
