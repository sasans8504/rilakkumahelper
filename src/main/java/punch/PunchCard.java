package punch;

import bin.RilakkumaCore;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.Properties;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import static bin.RilakkumaTray.*;
import static utils.Properties.*;

public enum PunchCard {
    INSTANCE;
    final String loginUrl = "https://auth.mayohr.com/HRM/Account/Login";
    final String punchCardUrl = "https://hrm.mayohr.com/ta/personal/checkin/checkinrecords/workrecords";
    final String cssUserName = "#root > div > div.container.top-256 > div.main-container > div > div:nth-child(2) > div > div > form > div:nth-child(1) > input";
    final String cssPassword = "#root > div > div.container.top-256 > div.main-container > div > div:nth-child(2) > div > div > form > div.div-input-icon.marginT-20 > input";
    final String cssLoginButton = "#root > div > div.container.top-256 > div.main-container > div > div:nth-child(2) > div > div > form > button";
    final String cssNotice = "#root > div > div.container.top-256 > div.main-container > div > div:nth-child(2) > div > div > form > div.div-input-icon.marginT-20 > label";
    final String cssLastPunchRecord = "#root > div > div > div > div.main-container > div > div > div > div.ta_cotent > div > div > div.ta_main_content_right > div > div.work-records-content > div:nth-child(3) > div.ta_grid_table > div > div.ta-scrollbar_wrapper.ta_grid_table > table > tbody > tr:nth-child(1)";
    final String cssDate = "#root > div > div > div > div.main-container > div > div > div > div.ta_cotent > div > div > div.ta_main_content_right > div > div.work-records-content > div:nth-child(3) > div.ta_grid_table > div > div.ta-scrollbar_wrapper.ta_grid_table > table > tbody > tr:nth-child(1) > td:nth-child(1)";
    final String cssTime = "#root > div > div > div > div.main-container > div > div > div > div.ta_cotent > div > div > div.ta_main_content_right > div > div.work-records-content > div:nth-child(3) > div.ta_grid_table > div > div.ta-scrollbar_wrapper.ta_grid_table > table > tbody > tr:nth-child(1) > td:nth-child(2)";
    WebDriver driver;

    public void login() {
        lockMenu(Properties.MENU_PUNCH);
        RilakkumaCore.INSTANCE.networking();
        try {
            System.setProperty(CHROME_PROPERTY, CHROME_DRIVER_PATH);
            ChromeOptions options = new ChromeOptions();
            options.addArguments(CHROME_HEADLESS);
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(loginUrl);
            System.out.println("connect to login page...");
            TimeUnit.SECONDS.sleep(2);
            System.out.println("typing user info...");
            WebElement username = driver.findElement(By.cssSelector(cssUserName));
            username.sendKeys(Properties.getSavedAccount());
            TimeUnit.SECONDS.sleep(2);
            WebElement userPassword = driver.findElement(By.cssSelector(cssPassword));
            userPassword.sendKeys(Properties.getSavedPassword());
            WebElement loginButton = driver.findElement(By.cssSelector(cssLoginButton));
            loginButton.click();
            System.out.println("wait submit redirect...");
            TimeUnit.SECONDS.sleep(7);
            System.out.println("check login notice...");
            List<WebElement> notice = driver.findElements(By.cssSelector(cssNotice));
            if (notice.isEmpty()) {
                saveCookie();
                driver.get(punchCardUrl);
                System.out.println("success to login, redirecting...");
                TimeUnit.SECONDS.sleep(4);

                String date = driver.findElement(By.cssSelector(cssDate)).getText();
                String time = driver.findElement(By.cssSelector(cssTime)).getText();
                System.out.println("date = " + date);
                System.out.println("time = " + time);
                xeMsg.put(DATA_DATE, date);

                time = time.split("/")[0];
                String calendar = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
                System.out.println("time = " + time);
                if (date.equalsIgnoreCase(calendar)) {
                    xeMsg.put(DATA_IN, time);
                    System.out.println("date is right");
                    int clock = Integer.parseInt(time.split(":")[0]);
                    System.out.println("punch in time = " + clock);
                    clock += 9;
                    time = clock + time.split(":")[1];
                    System.out.println("punch out time = " + time);
                    xeMsg.put(DATA_OUT, time);
                    xeMsg.put(DATA_CODE, CODE_SUCCESS);
                    xeMsg.remove(DATA_ERROR);
                } else {
                    System.out.println("can't get punch in today, check if something wrong?");
                    System.out.println("calendar = " + calendar);
                    System.out.println("date = " + date);
                    xeMsg.put(DATA_CODE, CODE_WAIT_HALF);
                    xeMsg.put(DATA_ERROR, "can't get punch in today, last date on punch is :" + date);
                    screenShot();
                }
            } else {
                System.out.println("failed to login");
                xeMsg.put(DATA_CODE, CODE_RETRY);
                xeMsg.put(DATA_ERROR, notice.get(0).getText());
                System.out.println("msg:" + xeMsg.get(DATA_ERROR));
                screenShot();
            }
        } catch (Exception e) {
            xeMsg.put(DATA_CODE, CODE_STOP);
            xeMsg.put(DATA_ERROR, "exception happend");
            screenShot();
            e.printStackTrace();
        } finally {
            if (driver != null)
                driver.quit();
            RilakkumaCore.INSTANCE.toLastStaticStatus();
            unlockMenu(Properties.MENU_PUNCH);
        }
    }

    private void screenShot() {
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile, new File("error_screen.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveCookie(){
        File file = new File("Cookies");
        try{
            file.delete();
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter Bwrite = new BufferedWriter(fileWriter);
            for(Cookie ck : driver.manage().getCookies())
            {
                Bwrite.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure()));
                Bwrite.newLine();
            }
            Bwrite.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean readCookie(){
        boolean hasCookie = false;
        try {
            File file = new File("Cookies.data");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String strLine;
            while ((strLine = bufferedReader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(strLine, ";");
                while (token.hasMoreTokens()) {
                    String name = token.nextToken();
                    String value = token.nextToken();
                    String domain = token.nextToken();
                    String path = token.nextToken();
                    Date expiry = null;

                    String val;
                    if (!(val = token.nextToken()).equals("null")) {
                        expiry = new Date(val);
                    }
                    boolean isSecure = Boolean.parseBoolean(token.nextToken());
                    Cookie ck = new Cookie(name, value, domain, path, expiry, isSecure);
                    System.out.println(ck);
                    driver.manage().addCookie(ck);
                    hasCookie = true;
                }
            }
            fileReader.close();
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hasCookie;
    }
}
