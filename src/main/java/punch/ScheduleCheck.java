package punch;

import bin.RilakkumaCore;
import net.sf.json.JSONObject;
import utils.Properties;
import utils.RDialog;

import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static utils.Properties.*;

public class ScheduleCheck implements Runnable {
    public static final String today = "today";
    public static final String tomorrow = "tomorrow";
    public static final String hour = "13";
    public static final String min = "00";
    public static boolean finish = false;
    public static String code = CODE_RETRY;
    public static boolean waitNotice = false;
    public static ScheduledExecutorService scheduledExecutorService;
    PunchCard punchCard = PunchCard.INSTANCE;
    private int retry = 12;
    private boolean scheduleStop = false;
    private static boolean running = false;

    MenuItem menuItem;
    String label;

    public ScheduleCheck(MenuItem menuItem, String label){
        this(menuItem, label, false);
    }

    public ScheduleCheck(MenuItem menuItem, String label, boolean force){
        if(force){
            removeXeMsgData();
            waitNotice = false;
        }
        this.menuItem = menuItem;
        this.label = label;
    }

    @Override
    public void run() {
        do {
            running = true;
            if (waitNotice) {
                RilakkumaCore.INSTANCE.success();
                RDialog.showMessageDialog(null, "今天下班時間是:" + Properties.getMsgOUT());
                RilakkumaCore.INSTANCE.toLastStaticStatus();
                code = CODE_STOP;
            } else {
                if (isMsgEmpty()) {
                    punchCard.login();
                } else {
                    if (!isMsgToday()) {
                        punchCard.login();
                    } else if (!getMsgCODE().equals(CODE_SUCCESS)) {
                        punchCard.login();
                    }
                }
                code = getMsgCODE();
                if (code.equals(CODE_STOP)) {
                    RDialog.showMessageDialog(null,
                            "由於不明原因無法取得下班時間，最後的錯誤訊息是:"
                                    + Properties.getMsgERROR()
                                    + "請稍等一陣子後手動取得，小熊熊感謝你");
                } else if (!code.equals(CODE_SUCCESS)) {
                    retry--;
                    if (1 == Properties.checkDayOfWeek() || 7 == Properties.checkDayOfWeek()) {
                        System.out.println("stop check on sunday and saturday");
                        retry = 0;
                    }
                    if (retry <= 0) {
                        scheduleStop = true;
                        RDialog.showMessageDialog(null,
                                "由於不明原因無法取得下班時間，最後的錯誤訊息是:"
                                        + Properties.getMsgERROR()
                                        + "請稍等一陣子後手動取得，小熊熊感謝你");
                        code = CODE_STOP;
                    }
                }

                if (code.equals(CODE_SUCCESS)) {
                    JSONObject json = new JSONObject();
                    json.put(DATA_AUTO_CHECK, isAutoCheckOutTime());
                    json.put(DATA_CODE, code);
                    json.put(DATA_DATE, getMsgDATE());
                    json.put(DATA_OUT, getMsgOUT());
                    Properties.writeXE(json);
                }
            }
            try {
                System.out.println("code = " + code + "error:" + Properties.getMsgERROR());
                System.out.println(Calendar.getInstance().getTime());
                switch (code) {
                    case CODE_RETRY:
                        System.out.println("retry at 5 minute");
                        TimeUnit.MINUTES.sleep(5);
                        break;
                    case CODE_WAIT_HALF:
                        System.out.println("retry at 30 minute");
                        TimeUnit.MINUTES.sleep(30);
                        break;
                    case CODE_WAIT_HOUR:
                        System.out.println("retry at 1 hour");
                        TimeUnit.HOURS.sleep(1);
                        break;
                    case CODE_STOP:
                        finish = true;
                        waitNotice = false;
                        retry = 12;
                        System.out.println("schedule stop");
                        break;
                    case CODE_SUCCESS:
                        finish = true;
                        retry = 12;
                        waitNotice = true;
                        System.out.println("schedule success");
                        break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (!finish);
        scheduledExecutorService.shutdown();
        if (scheduleStop) {
            running = false;
        } else {
            scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            scheduledExecutorService.schedule(this, tomorrowInitDelay(), TimeUnit.SECONDS);
        }

        if (waitNotice) {
            scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            scheduledExecutorService.schedule(this, getInitDelay("17", "50"), TimeUnit.SECONDS);
        }
    }

    public static boolean isRunning() {
        return running;
    }

    public static long tomorrowInitDelay() {
        return getInitDelay(tomorrow, hour, min);
    }

    public static long getInitDelay() {
        return getInitDelay(today, hour, min);
    }

    public static long getInitDelay(String hour, String min) {
        return getInitDelay(today, hour, min);
    }

    public static long getInitDelay(String day, String hour, String min) {
        Calendar calendar = Calendar.getInstance();
        if (day.equals(tomorrow)) {
            calendar.setTime(calendar.getTime());
            calendar.add(Calendar.DATE, 1);
        }
        SimpleDateFormat myFmt = new SimpleDateFormat("yyyy-MM-dd ");
        String time = myFmt.format(calendar.getTime());
        time = time + hour + ":" + min;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date;
            date = sdf.parse(time);
            long longDate = date.getTime();
            longDate -= System.currentTimeMillis();
            longDate /= 1000;
            return longDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void modifyLabel(){
        menuItem.setLabel(label);
    }
}
