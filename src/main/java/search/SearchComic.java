package search;

import bin.RilakkumaCore;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Properties;
import utils.RDialog;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static bin.RilakkumaTray.*;
import static utils.Properties.*;

public class SearchComic {
    private static WebDriver driver;

    public static void onePunchMan() {
        lockMenu(MENU_COMIC_ONEPUNCHMAN);
        RilakkumaCore.INSTANCE.networking();
        boolean checkAgain = true;
        int newestEpisode = getOnePunchManEpisode();
        int sinceEpisode = newestEpisode + 50;
        int untilEpisode = newestEpisode - 1;
        if (newestEpisode == -1)
            untilEpisode = -1 - newestEpisode;
        String onePunchMan;
        try {
            System.setProperty(CHROME_PROPERTY, CHROME_DRIVER_PATH);
            ChromeOptions options = new ChromeOptions();
            options.addArguments(CHROME_HEADLESS);
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            do {
                System.out.println("connect to url");
                onePunchMan = "https://tonarinoyj.jp/api/viewer/readable_products?current_readable_product_id=13932016480028985383&number_since=" + sinceEpisode + "&number_until=" + untilEpisode + "&read_more_num=250&series_id=13932016480028984490&type=episode";
                System.out.println(onePunchMan);
                driver.get(onePunchMan);
                System.out.println("get page source");
                String bodyStr = driver.getPageSource();
                for (int i = sinceEpisode - 1; i > untilEpisode; i--) {
                    if (bodyStr.contains("ページが見つかりません")) {
                        checkAgain = false;
                        break;
                    }
                    if (bodyStr.contains("[第" + i + "話]")) {
                        if (newestEpisode == i) {
                            checkAgain = false;
                            break;
                        }
                        newestEpisode = i;
                        sinceEpisode += 50;
                        untilEpisode += 50;
                        break;
                    }
                }
                TimeUnit.SECONDS.sleep(5);
            } while (checkAgain);
            RDialog.showMessageDialog(null, "最新話:" + newestEpisode);
            setOnePunchManEpisode(newestEpisode);
        } catch (Exception e) {
            RDialog.showMessageDialog(null, "取得失敗，請檢查截圖");
            screenShot();
            e.printStackTrace();
        } finally {
            if (driver != null)
                driver.quit();
            RilakkumaCore.INSTANCE.toLastStaticStatus();
            unlockMenu(MENU_COMIC_ONEPUNCHMAN);
        }
    }

    public static void onePiece() {
        lockMenu(MENU_COMIC_ONEPIECE);
        RilakkumaCore.INSTANCE.networking();
        String onePiece = "https://one-piece.cn/";
        try {
            System.setProperty(CHROME_PROPERTY, CHROME_DRIVER_PATH);
            ChromeOptions options = new ChromeOptions();
            options.addArguments(CHROME_HEADLESS);
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            System.out.println("connect to url");
            System.out.println(onePiece);
            driver.get(onePiece);
            TimeUnit.SECONDS.sleep(10);
            System.out.println("get page source");
            WebElement newestEpisode = driver.findElement(By.cssSelector("#main-area > article > div > div > div > div.newslist-column2.right > ul > li:nth-child(1) > a"));
            String newestTitle = newestEpisode.getText();
            String newestUrl = newestEpisode.getAttribute("href");
            int newestNum = findNumber(newestTitle.split("第")[1]);
            int ifSetUserInfo = RDialog.showConfirmDialog(null, "最新話: " + newestTitle + "\n要現在去看嗎?", "最新話", RDialog.OK_CANCEL_OPTION);
            if (ifSetUserInfo == RDialog.OK_OPTION) Desktop.getDesktop().browse(new URI(newestUrl));
            setComicInfo(DATA_ONEPIECE_NEWEST, newestNum);
        } catch (Exception e) {
            RDialog.showMessageDialog(null, "取得失敗，請檢查截圖");
            screenShot();
            e.printStackTrace();
        } finally {
            if (driver != null)
                driver.quit();
            RilakkumaCore.INSTANCE.toLastStaticStatus();
            unlockMenu(MENU_COMIC_ONEPIECE);
        }
    }

    private static void screenShot() {
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile, new File("one punch man error.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int findNumber(String str){
        String regex = "\\d*";
        Pattern p = Pattern.compile(regex);

        Matcher m = p.matcher(str);

        if(m.find()){
            return Integer.parseInt(m.group());
        }else{
            return 0;
        }
    }
}
