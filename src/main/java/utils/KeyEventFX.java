package utils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;

public class KeyEventFX<T extends Control> implements ChangeListener {
    private boolean ignore;
    T control;
    int maxLength;
    String restriction = "[0-9]";

    public KeyEventFX(T control) {
        this(control, 2);
    }

    public KeyEventFX(T control, int maxLength) {
        this.control = control;
        this.maxLength = maxLength;
    }

    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
        if (ignore || newValue == null) {
            return;
        }

        if (control instanceof ComboBox) {
            if (newValue.toString().length() > maxLength) {
                ignore = true;
                ((ComboBox)control).getEditor().setText(
                        newValue.toString().substring(0, maxLength));
                ignore = false;
            }

            if (!newValue.toString().matches(restriction + "*")) {
                ignore = true;
                ((ComboBox)control).getEditor().setText(oldValue.toString());
                ignore = false;
            }
        }
    }
}
