package utils;

import net.sf.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public enum Properties {
    INSTANCE;
    public static final String BASE_CORE = "core_data";
    public static final String BASE_XE = "xe_data";
    public static final String BASE_CRAW = "craw_data";
    public static final String MSG_EMPTY = "no data";
    public static final String USER_INFO = "user_data.txt";
    public static final String CHROME_PROPERTY = "webdriver.chrome.driver";
    public static final String CHROME_DRIVER_PATH = "chromedriver.exe";
    public static final String CHROME_HEADLESS = "--headless";
    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_FAIL = "fail";
    public static final String DATA_ACCOUNT = "savedAccount";
    public static final String DATA_PASSWORD = "savedPassword";
    public static final String DATA_DATE = "date";
    public static final String DATA_IN = "in";
    public static final String DATA_OUT = "out";
    public static final String DATA_CODE = "code";
    public static final String DATA_ERROR = "error";
    public static final String DATA_AUTO_CHECK = "autoCheckOut";
    public static final String DATA_ONEPUNCHMAN_NEWEST = "one_punch_man";
    public static final String DATA_ONEPIECE_NEWEST = "one_piece";
    public static final String DATA_THEME = "theme";
    public static final String DATA_THEME_SIZE = "size";
    public static final String CODE_SYS_STOP = "-5";
    public static final String CODE_STOP = "-1";
    public static final String CODE_SUCCESS = "0";
    public static final String CODE_RETRY = "1";
    public static final String CODE_WAIT_HALF = "2";
    public static final String CODE_WAIT_HOUR = "3";
    public static final String MENU_PUNCH = "punch_menu";
    public static final String MENU_COMIC_ONEPUNCHMAN = "onepunchman_menu";
    public static final String MENU_COMIC_ONEPIECE = "onepiece_menu";

    public static final String IMAGE_SIZE_LARGE = "large";
    public static final String IMAGE_SIZE_MIDDLE = "middle";
    public static final String IMAGE_SIZE_SMALL = "small";
    public static final String IMAGE_CHAIRO = "chairo";
    public static final String IMAGE_CAPOO = "capoo";
    public static final String IMAGE_GIF = ".gif";
    public static final String IMAGE_PNG = ".png";
    public static final String IMAGE_SPECIAL = "special";
    public static final String IMAGE_SPECIAL_RIGHT = "special";
    public static final String IMAGE_SPECIAL_LEFT = "special2";
    public static final String IMAGE_LEFT = "walk";
    public static final String IMAGE_RIGHT = "walk2";
    public static final String IMAGE_SIT = "sit";
    public static final String IMAGE_SIT_RIGHT = "sit2";
    public static final String IMAGE_NETWORKING = "networking";
    public static final String IMAGE_SUCCESS = "success";
    public static final String IMAGE_FAILED = "failed";
    public static final String IMAGE_COUNTDOWN = "countdown";
    public static final String IMAGE_ICON = "icon";
    public static final String[] IMAGE_RESOURCES = {IMAGE_SPECIAL, IMAGE_LEFT, IMAGE_RIGHT, IMAGE_SIT, IMAGE_SIT_RIGHT, IMAGE_NETWORKING, IMAGE_SUCCESS, IMAGE_FAILED, IMAGE_COUNTDOWN, IMAGE_ICON};

    private static String osName = "";
    private static boolean autoCheckOutTime = false;
    private static boolean event131 = false;
    private static int event131Year = 317;
    public static Map<String, String> xeMsg = new HashMap<>();
    public static Map<String, URL> resources = new HashMap<>();
    public static String theme = IMAGE_CHAIRO;
    public static String themeSize = IMAGE_SIZE_LARGE;

    private String dataStr = "";
    private static JSONObject allData;
    private static JSONObject xeData = new JSONObject();
    private static JSONObject crawData = new JSONObject();
    private static JSONObject coreData = new JSONObject();
    private static String savedAccount = "";
    private static String savedPassword = "";

    Properties() {
        File file = new File(USER_INFO);
        StringBuffer sb = new StringBuffer();
        if (file.exists()) {
            try {
                FileReader reader = new FileReader(USER_INFO);
                int str;
                while ((str = reader.read()) != -1) {
                    sb.append((char) str);
                }
                System.out.println(sb);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dataStr = sb.toString();
    }

    public void init() {
        osName = System.getProperty("os.name");
        event131();
        if ("".equals(dataStr)) dataStr = "{}";
        allData = JSONObject.fromObject(dataStr);

        if (!allData.isEmpty()) {
            if (allData.has(BASE_XE)) {
                xeData = allData.getJSONObject(BASE_XE);
                savedAccount = xeData.optString(DATA_ACCOUNT, "");
                savedPassword = xeData.optString(DATA_PASSWORD, "");
                autoCheckOutTime = xeData.optBoolean(DATA_AUTO_CHECK, false);
                xeMsg.put(DATA_CODE, xeData.optString(DATA_CODE, CODE_RETRY));
                xeMsg.put(DATA_DATE, xeData.optString(DATA_DATE, "0000/00/00"));
                xeMsg.put(DATA_OUT, xeData.optString(DATA_OUT, "00:00"));
            }
            if (allData.has(BASE_CRAW)) {
                crawData = allData.getJSONObject(BASE_CRAW);
            }
            if (allData.has(BASE_CORE)) {
                coreData = allData.getJSONObject(BASE_CORE);
                theme = coreData.optString(DATA_THEME, IMAGE_CHAIRO);
                themeSize = coreData.optString(DATA_THEME_SIZE, IMAGE_SIZE_LARGE);
            }
        }

        loadResources();
    }

    private void event131() {
        System.out.println("event131 check");
        if (Calendar.getInstance().get(Calendar.MONTH) == Calendar.JANUARY &&
                Calendar.getInstance().get(Calendar.DATE) == 31) {
            event131 = true;
            event131Year = Calendar.getInstance().get(Calendar.YEAR) - 2019;
        }
    }

    public static String saveUserInfo(String account, char[] password) {
        xeData.put(DATA_ACCOUNT, account);
        xeData.put(DATA_PASSWORD, String.valueOf(password));
        refreshInfo();
        return writeX(BASE_XE, xeData);
    }

    public static String writeXE(JSONObject json){
        for(Object key: json.keySet()){
            xeData.put(key, json.get(key));
        }
        return writeX(BASE_XE, xeData);
    }

    public static String writeXE(Object key, Object value) {
        xeData.put(key, value);
        System.out.println("writeXE calling");
        return writeX(BASE_XE, xeData);
    }

    public static String writeC(String key, String value) {
        if (key.equals(DATA_THEME))
            theme = value;
        coreData.put(key, value);
        System.out.println("writeC calling");
        return writeX(BASE_CORE, coreData);
    }

    public static String writeX(String base, JSONObject data) {
        try {
            allData.put(base, data);
            System.out.println("allData = " + allData);
            System.out.println("file writing");
            FileWriter fileWriter = new FileWriter(USER_INFO);
            fileWriter.write(allData.toString());
            fileWriter.close();
            System.out.println("write complete");
            return STATUS_SUCCESS;
        } catch (IOException ex) {
            ex.printStackTrace();
            return STATUS_FAIL;
        }
    }

    public static void refreshInfo() {
        savedAccount = xeData.optString(DATA_ACCOUNT, "");
        savedPassword = xeData.optString(DATA_PASSWORD, "");
    }

    public static boolean isUserInfoEmpty() {
        return "".equalsIgnoreCase(savedAccount) || "".equalsIgnoreCase(savedPassword);
    }

    public static void removeXeMsgData(){
        xeMsg.remove(DATA_AUTO_CHECK);
        xeMsg.remove(DATA_CODE);
        xeMsg.remove(DATA_DATE);
        xeMsg.remove(DATA_OUT);
    }

    public static String getSavedAccount() {
        return savedAccount;
    }

    public static String getSavedPassword() {
        return savedPassword;
    }

    public static String getMsgOUT() {
        if (isMsgEmpty()) return MSG_EMPTY;
        return xeMsg.get(DATA_OUT);
    }

    public static String getMsgDATE() {
        if (isMsgEmpty()) return MSG_EMPTY;
        return xeMsg.get(DATA_DATE);
    }

    public static String getMsgERROR() {
        if (isMsgEmpty()) return MSG_EMPTY;
        if (!xeMsg.containsKey(DATA_ERROR)) return "no error";
        return xeMsg.get(DATA_ERROR);
    }

    public static String getMsgCODE() {
        return xeMsg.get(DATA_CODE);
    }

    public static String getOsName() {
        return osName;
    }

    public static boolean isMsgToday() {
        if (isMsgEmpty()) return false;
        if (!xeMsg.containsKey(DATA_DATE)) return false;
        return xeMsg.get(DATA_DATE).equalsIgnoreCase(new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
    }

    public static boolean isMsgEmpty() {
        return xeMsg.isEmpty();
    }

    public static boolean isAutoCheckOutTime() {
        return autoCheckOutTime;
    }

    public static void setAutoCheckOutTime(boolean autoCheckOutTime) {
        Properties.autoCheckOutTime = autoCheckOutTime;
        writeXE(DATA_AUTO_CHECK, autoCheckOutTime);
    }

    public static int checkDayOfWeek() {
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    }

    public static void setOnePunchManEpisode(int newestEpisode) {
        crawData.put(DATA_ONEPUNCHMAN_NEWEST, newestEpisode);
        writeX(BASE_CRAW, crawData);
    }

    public static void setComicInfo(String key, int newestEpisode) {
        crawData.put(key, newestEpisode);
        writeX(BASE_CRAW, crawData);
    }

    public static int getOnePunchManEpisode() {
        if (!crawData.has(DATA_ONEPUNCHMAN_NEWEST)) return -1;
        return crawData.getInt(DATA_ONEPUNCHMAN_NEWEST);
    }

    public static int getOnePieceEpisode() {
        if (!crawData.has(DATA_ONEPIECE_NEWEST)) return -1;
        return crawData.getInt(DATA_ONEPIECE_NEWEST);
    }

    public static void loadResources(String theme) {
        loadResources(theme, themeSize);
    }

    public static void loadResources() {
        loadResources(theme, themeSize);
    }

    public static void loadResources(String theme, String size) {
        if(!themeSize.equalsIgnoreCase(size))
            themeSize = size;
        for (String key :
                IMAGE_RESOURCES) {
            if (key.equals(IMAGE_ICON)) {
                resources.put(key, Objects.requireNonNull(Properties.class.getClassLoader().getResource(theme + "/" + key + IMAGE_PNG)));
                continue;
            } else if (key.equals(IMAGE_COUNTDOWN)) {
                resources.put(key, Objects.requireNonNull(Properties.class.getClassLoader().getResource(theme + "/" + key + IMAGE_GIF)));
                continue;
            } else if (key.equals(IMAGE_SPECIAL)) {
                if (theme.equals(IMAGE_CHAIRO)) {
                    resources.put(IMAGE_SPECIAL_RIGHT, Objects.requireNonNull(Properties.class.getClassLoader().getResource(theme + "/" + size + "/" + IMAGE_SPECIAL_RIGHT + IMAGE_GIF)));
                    resources.put(IMAGE_SPECIAL_LEFT, Objects.requireNonNull(Properties.class.getClassLoader().getResource(theme + "/" + size + "/" + IMAGE_SPECIAL_LEFT + IMAGE_GIF)));
                    continue;
                } else if (theme.equals(IMAGE_CAPOO)) {
                    resources.put(key, Objects.requireNonNull(Properties.class.getClassLoader().getResource(theme + "/" + size + "/" + key + IMAGE_GIF)));
                    continue;
                }
            }
            resources.put(key, Objects.requireNonNull(Properties.class.getClassLoader().getResource(theme + "/" + size + "/" + key + IMAGE_GIF)));
        }
    }

    public static boolean isEvent131() {
        return event131;
    }

    public static int getEvent131Year() {
        return event131Year;
    }
}