package utils;

import bin.RilakkumaCore;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RClock extends JFrame {
    final String NAME_START = "start";
    final String NAME_CONTINUE = "continue";
    final String NAME_PAUSE = "pause";
    final String NAME_CLEAR = "clear";
    final String NAME_STOP = "stop";
    final String STATUS_WAIT = "wait";
    final String STATUS_COUNT = "countdown";
    private static RClock instance;
    String status = STATUS_WAIT;
    private Timer timer;
    private int textHeight = 140;
    private int buttonHeight = textHeight + 50;

    public static RClock getInstance() {
        if (instance == null) instance = new RClock();
        else if (!instance.isVisible()) instance.setVisible(true);
        return instance;
    }

    RClock() {
        setSize(280, 270);
        setResizable(false);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setTitle("倒數計時");

        JLabel icon = new JLabel(new ImageIcon(Properties.resources.get(Properties.IMAGE_COUNTDOWN)));
        icon.setBounds(10, 10, 256, 118);
        JLabel hourT = new JLabel("00");
        hourT.setFont(new Font("consolas", Font.PLAIN, 30));
        hourT.setBounds(30, textHeight, 60, 40);
        JLabel minuteT = new JLabel("00");
        minuteT.setFont(new Font("consolas", Font.PLAIN, 30));
        minuteT.setBounds(110, textHeight, 60, 40);
        JLabel secondT = new JLabel("00");
        secondT.setFont(new Font("consolas", Font.PLAIN, 30));
        secondT.setBounds(190, textHeight, 60, 40);
        hourT.setVisible(false);
        minuteT.setVisible(false);
        secondT.setVisible(false);

        JPanel panel = new JPanel();
        panel.setLayout(null);
        JComboBox<String> hour = comboBoxG(true);
        hour.setBounds(20, textHeight, 60, 40);
        hour.getEditor().addActionListener(e -> isLegal(e.getActionCommand(), hour));
        JLabel colon1 = new JLabel(":");
        colon1.setFont(new Font("consolas", Font.PLAIN, 24));
        colon1.setBounds(80, textHeight, 20, 40);
        JComboBox<String> minute = comboBoxG();
        minute.setBounds(100, textHeight, 60, 40);
        minute.getEditor().addActionListener(e -> isLegal(e.getActionCommand(), minute));
        JLabel colon2 = new JLabel(":");
        colon2.setFont(new Font("consolas", Font.PLAIN, 24));
        colon2.setBounds(160, textHeight, 20, 40);
        JComboBox<String> second = comboBoxG();
        second.setBounds(180, textHeight, 60, 40);
        second.getEditor().addActionListener(e -> isLegal(e.getActionCommand(), second));
        JButton start = new JButton("開始");
        start.setName(NAME_START);
        start.setBounds(30, buttonHeight, 60, 40);
        JButton clear = new JButton("清除");
        clear.setName(NAME_CLEAR);
        clear.setBounds(170, buttonHeight, 60, 40);
        start.addActionListener(e -> {
            if (start.getName().equals(NAME_START)) {
                hour.setVisible(false);
                minute.setVisible(false);
                second.setVisible(false);
                isLegal(hour.getEditor().getItem().toString(), hour);
                isLegal(minute.getEditor().getItem().toString(), minute);
                isLegal(second.getEditor().getItem().toString(), second);
                hourT.setText(hour.getEditor().getItem().toString());
                minuteT.setText(minute.getEditor().getItem().toString());
                secondT.setText(second.getEditor().getItem().toString());
                hourT.setVisible(true);
                minuteT.setVisible(true);
                secondT.setVisible(true);
                start.setText("暫停");
                start.setName(NAME_PAUSE);
                clear.setText("停止");
                clear.setName(NAME_STOP);
                status = STATUS_COUNT;
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (status.equals(STATUS_COUNT)) {
                            if (!secondT.getText().equals("00")) {
                                int secondR = Integer.parseInt(secondT.getText());
                                secondR--;
                                if (secondR < 10)
                                    secondT.setText("0" + secondR);
                                else
                                    secondT.setText("" + secondR);
                            } else if (!minuteT.getText().equals("00")) {
                                int minuteR = Integer.parseInt(minuteT.getText());
                                minuteR--;
                                if (minuteR < 10)
                                    minuteT.setText("0" + minuteR);
                                else
                                    minuteT.setText("" + minuteR);
                                secondT.setText("59");
                            } else if (!hourT.getText().equals("00")) {
                                int hourR = Integer.parseInt(hourT.getText());
                                hourR--;
                                if (hourR < 10)
                                    hourT.setText("0" + hourR);
                                else
                                    hourT.setText("" + hourR);
                                secondT.setText("59");
                                minuteT.setText("59");
                            } else {
                                status = STATUS_WAIT;
                                RilakkumaCore.INSTANCE.success();
                                RDialog.showMessageDialog(null, "時間到");
                                RilakkumaCore.INSTANCE.toLastStaticStatus();
                                hour.setVisible(true);
                                minute.setVisible(true);
                                second.setVisible(true);
                                hourT.setVisible(false);
                                minuteT.setVisible(false);
                                secondT.setVisible(false);
                                start.setText("開始");
                                start.setName(NAME_START);
                                clear.setText("清除");
                                clear.setName(NAME_CLEAR);
                                timer.cancel();
                            }
                        }
                    }
                }, 0, 1000);
            } else if (start.getName().equals(NAME_PAUSE)) {
                status = STATUS_WAIT;
                start.setText("繼續");
                start.setName(NAME_CONTINUE);
            } else if (start.getName().equals(NAME_CONTINUE)) {
                status = STATUS_COUNT;
                start.setText("暫停");
                start.setName(NAME_PAUSE);
            }
        });
        clear.addActionListener(e -> {
            if (clear.getName().equals(NAME_CLEAR)) {
                hour.getEditor().setItem("00");
                minute.getEditor().setItem("00");
                second.getEditor().setItem("00");
            }
            if (clear.getName().equals(NAME_STOP)) {
                status = STATUS_WAIT;
                timer.cancel();
                hour.setVisible(true);
                minute.setVisible(true);
                second.setVisible(true);
                hourT.setVisible(false);
                minuteT.setVisible(false);
                secondT.setVisible(false);
                start.setText("開始");
                start.setName(NAME_START);
                clear.setText("清除");
                clear.setName(NAME_CLEAR);
            }
        });

        panel.add(icon);
        panel.add(hourT);
        panel.add(hour);
        panel.add(colon1);
        panel.add(minuteT);
        panel.add(minute);
        panel.add(colon2);
        panel.add(secondT);
        panel.add(second);
        panel.add(start);
        panel.add(clear);
        add(panel);
        setVisible(true);
    }

    private JComboBox<String> comboBoxG() {
        return comboBoxG(false);
    }

    private JComboBox<String> comboBoxG(boolean isHour) {
        String[] possibles;
        if (isHour) {
            possibles = new String[100];
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    possibles[i * 10 + j] = i + "" + j;
                }
            }
        } else {
            possibles = new String[60];
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 10; j++) {
                    possibles[i * 10 + j] = i + "" + j;
                }
            }
        }

        JComboBox<String> temp = new JComboBox<>(possibles);
        temp.setEditable(true);
        temp.setFont(new Font("consolas", Font.PLAIN, 24));
        return temp;
    }

    private void isLegal(String value, JComboBox<String> box) {
        if (isNumeric(value)) {
            if (value.length() > 2) {
                box.getEditor().setItem(value.substring(0, 2));
            }
        } else {
            box.getEditor().setItem("00");
        }
    }

    private boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    private void countdown() {

    }
}
