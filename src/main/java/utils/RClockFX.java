package utils;
//
//import javafx.animation.Animation;
//import javafx.animation.KeyFrame;
//import javafx.animation.Timeline;
//import javafx.application.Application;
//import javafx.application.Platform;
//import javafx.beans.value.ChangeListener;
//import javafx.collections.FXCollections;
//import javafx.event.EventHandler;
//import javafx.geometry.Insets;
//import javafx.geometry.Pos;
//import javafx.scene.Scene;
//import javafx.scene.control.*;
//import javafx.scene.layout.GridPane;
//import javafx.scene.text.Font;
//import javafx.scene.text.FontWeight;
//import javafx.scene.text.Text;
//import javafx.stage.Stage;
//import javafx.stage.WindowEvent;
//import javafx.util.Duration;
//
//import java.util.Timer;
//import java.util.TimerTask;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//
//public class RClockFX extends Application {
//    Text hourT;
//    Text minuteT;
//    Text secondT;
//    ComboBox<String> hour;
//    ComboBox<String> minute;
//    ComboBox<String> second;
//    Text colon1;
//    Text colon2;
//    Timer timer;
//    Button begin;
//    Button reset;
//    Timeline timeline;
//
//    enum Status {RUN, PAUSE, STOP}
//    Status status = Status.STOP;
//    boolean pause = false;
//
//    @Override
//    public void start(Stage primaryStage) throws Exception {
//        primaryStage.setAlwaysOnTop(true);
//        Properties.setFxLaunched(true);
//        primaryStage.setOnCloseRequest(t -> {
//            System.out.println("exit");
//            Properties.setFxLaunched(false);
//            primaryStage.close();
//            Platform.exit();
//        });
//
//        GridPane grid = new GridPane();
//        grid.setAlignment(Pos.CENTER);
//        grid.setPadding(new Insets(25, 25, 25, 25));
//
//        Scene scene = new Scene(grid, 300, 150);
//        primaryStage.setScene(scene);
//        primaryStage.show();
//
//        hourT = generateText("00", false);
//        minuteT = generateText("00", false);
//        secondT = generateText("00", false);
//
//        grid.add(hourT, 0, 0);
//        grid.add(minuteT, 2, 0);
//        grid.add(secondT, 4, 0);
//
//        hour = generateCombobox(true);
//        colon1 = generateText(":");
//        minute = generateCombobox();
//        colon2 = generateText(":");
//        second = generateCombobox();
//
//        grid.add(hour, 0, 0);
//        grid.add(colon1, 1, 0);
//        grid.add(minute, 2, 0);
//        grid.add(colon2, 3, 0);
//        grid.add(second, 4, 0);
//
//        begin = new Button("開始");
//        begin.setId("start");
//        begin.setFont(Font.font("Consolas", FontWeight.NORMAL, 15));
//        grid.add(begin, 0, 2);
//        reset = new Button("重置");
//        reset.setId("reset");
//        reset.setFont(Font.font("Consolas", FontWeight.NORMAL, 15));
//        reset.setAlignment(Pos.BASELINE_RIGHT);
//        grid.add(reset, 4, 2);
//
//        begin.setOnAction(event -> {
//            if (begin.getId().equals("start")) {
//                if (hour.getValue().equals("")) hour.setValue("00");
//                else if (hour.getValue().length() < 2) hour.setValue("0" + hour.getValue());
//                if (minute.getValue().equals("")) minute.setValue("00");
//                else if (minute.getValue().length() < 2) minute.setValue("0" + minute.getValue());
//                if (second.getValue().equals("")) second.setValue("00");
//                else if (second.getValue().length() < 2) second.setValue("0" + second.getValue());
//
//                counting();
//                begin.setText("暫停");
//                begin.setId("pause");
//                reset.setText("停止");
//                reset.setId("stop");
//            } else if (begin.getId().equalsIgnoreCase("pause")) {
//                setPause(true);
//                begin.setText("繼續");
//                begin.setId("continue");
//            } else if (begin.getId().equalsIgnoreCase("continue")) {
//                setPause(false);
//                begin.setText("暫停");
//                begin.setId("pause");
//            }
//        });
//
//        reset.setOnAction(event -> {
//            if (reset.getId().equals("reset")) {
//                hour.setValue("00");
//                minute.setValue("00");
//                second.setValue("00");
//            }else if(reset.getId().equals("stop")){
//                timeUp();
//                begin.setText("開始");
//                begin.setId("start");
//                reset.setText("重置");
//                reset.setId("reset");
//            }
//        });
//    }
//
//    private void counting() {
//        status = Status.RUN;
//        hour.setVisible(false);
//        minute.setVisible(false);
//        second.setVisible(false);
//
//        hourT.setText(hour.getValue());
//        minuteT.setText(minute.getValue());
//        secondT.setText(second.getValue());
//        hourT.setVisible(true);
//        minuteT.setVisible(true);
//        secondT.setVisible(true);
//
//        timeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
//            if (!pause) {
//                if (!secondT.getText().equals("00")) {
//                    int secondR = Integer.parseInt(secondT.getText());
//                    secondR--;
//                    if (secondR < 10)
//                        secondT.setText("0" + secondR);
//                    else
//                        secondT.setText("" + secondR);
//                } else if (!minuteT.getText().equals("00")) {
//                    int minuteR = Integer.parseInt(minuteT.getText());
//                    minuteR--;
//                    if (minuteR < 10)
//                        minuteT.setText("0" + minuteR);
//                    else
//                        minuteT.setText("" + minuteR);
//                    secondT.setText("59");
//                } else if (!hourT.getText().equals("00")) {
//                    int hourR = Integer.parseInt(hourT.getText());
//                    hourR--;
//                    if (hourR < 10)
//                        hourT.setText("0" + hourR);
//                    else
//                        hourT.setText("" + hourR);
//                    secondT.setText("59");
//                    minuteT.setText("59");
//                } else {
//                    status = Status.STOP;
//                    Alert alert = new Alert(Alert.AlertType.WARNING);
//                    alert.setHeaderText("時間到");
//                    alert.show();
//                    timeUp();
//                }
//            }
//        }));
//        timeline.setCycleCount(Animation.INDEFINITE);
//        timeline.play();
//    }
//
//    private void timeUp() {
//        status = Status.STOP;
//        timeline.stop();
//        hour.setVisible(true);
//        minute.setVisible(true);
//        second.setVisible(true);
//
//        hourT.setVisible(false);
//        minuteT.setVisible(false);
//        secondT.setVisible(false);
//
//        begin.setText("開始");
//        begin.setId("start");
//        reset.setText("重置");
//        reset.setId("reset");
//    }
//
//    private void setPause(boolean isPause) {
//        pause = isPause;
//    }
//
//    private ComboBox<String> generateCombobox() {
//        return generateCombobox(false);
//    }
//
//    private ComboBox<String> generateCombobox(boolean hour) {
//        String[] possibles;
//        if (hour) {
//            possibles = new String[100];
//            for (int i = 0; i < 10; i++) {
//                for (int j = 0; j < 10; j++) {
//                    possibles[i * 10 + j] = i + "" + j;
//                }
//            }
//        } else {
//            possibles = new String[60];
//            for (int i = 0; i < 6; i++) {
//                for (int j = 0; j < 10; j++) {
//                    possibles[i * 10 + j] = i + "" + j;
//                }
//            }
//        }
//
//        ComboBox<String> temp = new ComboBox<>();
//        temp.setEditable(true);
//        temp.getItems().addAll(possibles);
//        temp.getEditor().textProperty().addListener(new KeyEventFX(temp));
//        temp.setValue("00");
//
//        return temp;
//    }
//
//    private Text generateText(String text) {
//        return generateText(text, true);
//    }
//
//    private Text generateText(String text, boolean visible) {
//        Text temp = new Text(text);
//        temp.setFont(Font.font("Consolas", FontWeight.NORMAL, 30));
//        temp.setVisible(visible);
//        return temp;
//    }
//}


import javax.swing.*;
import java.awt.*;

public class RClockFX extends JFrame{
    private static RClockFX instance;

    public static RClockFX getInstance() {
        if(instance == null) instance = new RClockFX();
        return instance;
    }

    RClockFX(){
        setSize(500, 500);
        setResizable(false);
        setVisible(true);

        JLabel colon1 = new JLabel("：");
        colon1.setBounds(60, 20, 20, 30);
        add(colon1);

        String[] possibles = new String[100];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                possibles[i * 10 + j] = i + "" + j;
            }
        }

        JComboBox<String> temp = new JComboBox<>(possibles);
        temp.setEditable(true);
        temp.setPrototypeDisplayValue("00");
        temp.setFont(new Font("Consolas", Font.PLAIN, 15));
        temp.setSize(50, 30);
        temp.setLocation(10, 20);
        add(temp);
    }

    private JComboBox<String> generateCombobox(boolean hour) {
        String[] possibles;
        if (hour) {
            possibles = new String[100];
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    possibles[i * 10 + j] = i + "" + j;
                }
            }
        } else {
            possibles = new String[60];
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < 10; j++) {
                    possibles[i * 10 + j] = i + "" + j;
                }
            }
        }

        JComboBox<String> temp = new JComboBox<>(possibles);
        temp.setEditable(true);
        temp.setPrototypeDisplayValue("00");
        temp.setFont(new Font("Consolas", Font.PLAIN, 15));
        temp.setSize(50, 30);

        return temp;
    }
}