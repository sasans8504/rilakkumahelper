package utils;

import java.awt.*;

public class Vector2 {
    public int x;
    public int y;

    public Vector2() {
        x = 0;
        y = 0;
    }

    public Vector2(int x, int y) {
        setPosition(x, y);
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setPosition(Point point) {
        this.x = point.x;
        this.y = point.y;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    public Point toPoint(){
        return new Point(x, y);
    }
}
